require "telegram_bot"

token = '1750872988:AAEcH5u0GkSruL15lvhGGr6vqIRqQiVJqgo'

bot = TelegramBot.new(token: token)
bot.get_updates(fail_silently: true) do |message|
  puts "@#{message.from.username}: #{message.text}"
  command = message.get_command_for(bot)

  message.reply do |reply|
    case command
    when /start/i
      reply.text = "All I can do is say hello. Try the /greet command."
    when /greet/i
      reply.text = "Hello, #{message.from.first_name}. :D"
    when /socials/i
      reply.text = "Twitter: https://twitter.com/KaibaCorpBSC ; Facebook: https://www.facebook.com/groups/kaibacorpbsc"
    when /website/i
      reply.text = "https://kaibacorpbsc.com/"
    when /countdown/i
      reply.text = "https://www.timeanddate.com/countdown/launch?iso=20210526T14&p0=1440&msg=DH+Presale&font=cursive&csz=1"
    when /presale/i
      reply.text = "You can join the presale via dxsale. The link will be posted when countdown ends. You can connect with Trustwallet or Metamask"
    when /chart/i
      reply.text = "https://poocoin.app/tokens/0xd10cba9b83475a51b83791a60fee05c120b6cc51"
    when /contract/i
      reply.text = "🎆Dark Hole $DH contract: 0xD10Cba9B83475A51b83791a60fEE05C120b6cc51"
    else
      reply.text = "I have no idea what #{command.inspect}means."
    end
    puts "sending #{reply.text.inspect} to @#{message.from.username}"
    reply.send_with(bot)
  end
end
